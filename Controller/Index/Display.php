<?php
namespace Gamma\Hello\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Display extends Action
{
    public function __construct(
        Context $context)
    {
        return parent::__construct($context);
    }

    public function execute()
    {
        echo 'Hello World';
        exit;
    }
}